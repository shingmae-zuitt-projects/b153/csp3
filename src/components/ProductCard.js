// import {useState, useEffect} from 'react'
import { Link } from 'react-router-dom'
import {Card, Button} from 'react-bootstrap'

export default function ProductCard({productProp}){
	const {_id, name, description, price} = productProp

	return(
		<Card className="m-3">
			<Card.Body>
			    <Card.Title>{name}</Card.Title>
			    <Card.Subtitle>Description:</Card.Subtitle>
			    <Card.Text>{description}</Card.Text>
			    <Card.Subtitle>Price:</Card.Subtitle>
			    <Card.Text>{price}</Card.Text>
			    <Link className="btn btn-primary" to={`/products/${_id}`}>Details</Link>
{/*			    <Card.Text>Enrollees: {count}</Card.Text>
			    <Card.Text>Seats: {seats}</Card.Text>
			    {isOpen
			    	?
			    	<Button variant="primary" onClick={enroll}>Enroll</Button>
			    	:
			    	<Button variant="primary" disabled>Enroll</Button>
			    }*/}
			    
		  	</Card.Body>
		</Card>
	)
}
