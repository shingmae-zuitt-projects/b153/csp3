import Banner from '../components/Banner'
import Highlights from '../components/Highlights'

export default function Home(){

	const data = {
		title: "BTS Merch for All",
		content: "Store for your favorite BTS kpop idol",
		destination: "/products",
		label: "Search Products"
	}

	return(
	<>
		<Banner bannerProps={data}/>
		<Highlights/>
	</>
	)
}
