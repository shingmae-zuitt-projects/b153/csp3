let productData = [
	{
		id: "wdc001",
		name: "BTS mug",
		description: "High quality Bts inspired Mugs by independent artists and designers from around the world.",
		price: 500,
		onOffer: true
	}, 
	{
		id: "wdc002",
		name: "BTS Jimin Pillow",
		description: "Lorem ipsum dolor sit amet consectetur, adipisicing, elit. Ex officia magni vel saepe et vero, distinctio iustor, ducimus doloribus?",
		price: 1000,
		onOffer: true
	}, 
	{
		id: "wdc003",
		name: "BTS kpop stickers",
		description: "Ex officia magni vel saepe et vero, distinctio iusto doloremque, asperiores labore laboriosam quibusdam consectetur nam consequuntur nulla? Quidem consectetur, ducimus doloribus?",
		price: 45000,
		onOffer: true
	}, 
	{
		id: "wdc004",
		name: "BTS ",
		description: "Lorem ipsum dolor sit amet consectetur, adipisicing, elit. Ex officia magni vel saepe et vero, distinctio iusto doloremque, asperiores labore laboriosam quibusdam consectetur nam consequuntur nulla? Quidem consectetur, ducimus doloribus?",
		price: 45000,
		onOffer: false
	}
]

export default productData