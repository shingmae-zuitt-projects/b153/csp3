import { useState, useEffect } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { Container } from 'react-bootstrap';
import AppNavbar from './components/AppNavbar';
// import Banner from './components/Banner'
// import Highlights from './components/Highlights'
import Home from './pages/Home';
import Products from './pages/Products';
import SpecificProduct from './pages/SpecificProducts';
import Register from './pages/Register';
import Login from './pages/Login';
import Error from './pages/Error';
import { UserProvider } from './UserContext';
import './App.css';

//App.js is typically called the "top-level component"

function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  const unsetUser = () => {
    localStorage.clear()

    setUser({
      id: null,
      isAdmin: null
    })
  }

  useEffect(() => {

    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    })
    .then(res => res.json())
    .then(data => {
      //if we receive the user's data from our API, set the user state's values back to the user's details
      if(typeof data._id !== "undefined"){

        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })

      }else{ //if not, set the user state's values to null

        setUser({
          id: null,
          isAdmin: null
        })

      }
    })

  }, [])

  return (
  //UserProvider distributes our user state, setUser function, and unsetUser function to our entire app
  <UserProvider value={{user, setUser, unsetUser}}>
    <Router>
      <AppNavbar/>
      <Container>
        <Switch>
            <Route exact path="/" component={Home}/>
            {/* <Route exact path="/products" component={Products}/>
            <Route exact path="/products/:productId" component={SpecificProduct}/> */}
            <Route exact path="/register" component={Register}/>
            <Route exact path="/login" component={Login}/>
            <Route component={Error}/>
        </Switch>
      </Container>
    </Router>
  </UserProvider>
);
}

export default App;
